import { Injectable } from '@angular/core';
import { Item } from './model/Item.model';

import items from '../data/favoriteItems.json';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor() { }

  getItems(): Item[] {
    let processedItems = new Array<Item>();
    items.items.forEach(element => {
      processedItems.push(new Item(element.name, element.rating));
    });

    return processedItems;
  }
}
