import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { switchMap, filter, debounceTime } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import * as fromStore from '../reducers';

import {  } from 'rxjs';

import { ItemService } from '../../item.service';
import * as itemActions from '../actions/items.actions';

const RANDOM_RATINGS_PERIOD_MS = 500;

@Injectable()
export class AppEffects {
  constructor(
      private actions$: Actions,
      private itemService$: ItemService,
      private store: Store<fromStore.State>
    ) {}

    @Effect()
    loadItems$: Observable<Action> = this.actions$.pipe(
      ofType(itemActions.ItemsActionTypes.LoadItems),
      switchMap(() => {
        return Promise.resolve(new itemActions.SetItems(this.itemService$.getItems()));
      })
    );

    @Effect()
    togglePeriodicallyRandomRatings$: Observable<Action> = this.actions$.pipe(
      ofType(itemActions.ItemsActionTypes.ToggleAutoRating),
      switchMap(() => {
        return Promise.resolve(new itemActions.RepeatRandomRatings());
      })
    );

    @Effect()
    periodicallyAddRandomRatings$: Observable<Action> = this.actions$.pipe(
      ofType(itemActions.ItemsActionTypes.RepeatRandomRatings),
      switchMap(() => this.store.select(fromStore.isRandomRank)),
      filter((value: boolean) => value),
      debounceTime(RANDOM_RATINGS_PERIOD_MS),
      switchMap(() => [
        new itemActions.RepeatRandomRatings(),
        new itemActions.AddRandomRatings()
      ])
    );

}
