import * as itemActions from '../actions/items.actions';
import { Item } from '../../model/Item.model';

export interface State {
  items: Item[],
  randomRank: boolean
}

export const initialState: State = {
  items: [],
  randomRank: false
}

function rateItem(state: State, action: itemActions.RateItem){
  let newItems = new Array<Item>();

  newItems = state.items.map(item => {
    if (item.name === action.itemName) {
      return item.immutableRate(item.rate + action.rateDiff);
    } else {
      return item.clone();
    }
  });

  return {...state, items: sortItems(newItems)};
}

function sortItems(items: Item[]) {
   return items.slice().sort(function(firstEl: Item, secondEl: Item) {
     return secondEl.rate - firstEl.rate;
   });
}

const RANDOM_RATING_LIMIT = 2;
function getRandomIncrease(){
  return Math.round(Math.random() * RANDOM_RATING_LIMIT);
}

function addRandomRatings(state: State): State{
  let newItems = state.items.map(item => item.immutableRate(item.rate + getRandomIncrease())); 

  return {...state, randomRank: false, items: sortItems(newItems)};
}

export function reducer(state: State = initialState, action: itemActions.ItemsActions): State {
  switch (action.type) {
    case itemActions.ItemsActionTypes.SetItems:
      return {... state, items: sortItems(action.payload)};

    case itemActions.ItemsActionTypes.RateItem:
      return rateItem(state, action);

    case itemActions.ItemsActionTypes.ToggleAutoRating:
      return {...state, randomRank: !state.randomRank};

    case itemActions.ItemsActionTypes.AddRandomRatings:
      return addRandomRatings(state);

    case itemActions.ItemsActionTypes.AddRandomRatings:
      return addRandomRatings(state);

    default:
      return state;
  }
}

export const getItems = (state: State): Item[] => state.items;

export const isRandomRank = (state: State): boolean => state.randomRank;