import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import * as fromItems from './items.reducer';

export interface State {
  items: fromItems.State
}

export const initialState: State = {
  items: fromItems.initialState
}

export const reducers: ActionReducerMap<State> = {
  items: fromItems.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

export const selectStoreState = createFeatureSelector<fromItems.State>('items');
export const getItems = createSelector(selectStoreState, fromItems.getItems)
export const isRandomRank = createSelector(selectStoreState, fromItems.isRandomRank)