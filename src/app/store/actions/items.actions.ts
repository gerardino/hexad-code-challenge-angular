import { Action } from '@ngrx/store';
import { Item } from 'src/app/model/Item.model';

export enum ItemsActionTypes {
  LoadItems = '[Items] Load Items',
  SetItems = '[Items] Set Items',
  RateItem = '[Items] Rate an Item',
  ToggleAutoRating = '[Items] Randomize Item Ratings',
  AddRandomRatings = '[Items] Add Random Ratings',
  RepeatRandomRatings = '[Items] Repeat Random Ratings'

}

export class LoadItems implements Action {
  readonly type = ItemsActionTypes.LoadItems;
}

export class SetItems implements Action {
  readonly type = ItemsActionTypes.SetItems;

  constructor(public payload: Item[]){}
}

export class RateItem implements Action {
  readonly type = ItemsActionTypes.RateItem;

  constructor(public itemName: string, public rateDiff: number){}
}

export class ToggleAutoRating implements Action {
  readonly type = ItemsActionTypes.ToggleAutoRating;
}

export class AddRandomRatings implements Action {
  readonly type = ItemsActionTypes.AddRandomRatings;
}

export class RepeatRandomRatings implements Action {
  readonly type = ItemsActionTypes.RepeatRandomRatings;
}

export type ItemsActions = LoadItems | SetItems | RateItem | ToggleAutoRating | AddRandomRatings | RepeatRandomRatings;