import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../store/reducers';
import { Observable } from 'rxjs';
import { Item } from '../model/Item.model';
import * as itemActions from '../store/actions/items.actions';

const PLAY_HEX_SYMBOL = "\u25B6";
const PAUSE_HEX_SYMBOL = "\u23F8";

@Component({
  selector: 'app-list',
  templateUrl: './app-list.component.html',
  styleUrls: ['./app-list.component.scss']
})
export class AppListComponent implements OnInit {

  items$: Observable<Item[]>;
  randomRankText: string = PLAY_HEX_SYMBOL;

  constructor(private store: Store<fromStore.State>) { }

  ngOnInit() {
    this.items$ = this.store.select(fromStore.getItems);
    this.store.select(fromStore.isRandomRank).subscribe(value => {
      this.randomRankText = value ? PAUSE_HEX_SYMBOL : PLAY_HEX_SYMBOL;
    })
  }

  toggleRandomRating(){
    this.store.dispatch(new itemActions.ToggleAutoRating());
  }

}
