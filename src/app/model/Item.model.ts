export class Item {
  constructor(public name: string, public rate: number) { }

  immutableRate(newRating: number){
    return new Item(this.name, newRating);
  }

  clone(){
    return this.immutableRate(this.rate);
  }
}
