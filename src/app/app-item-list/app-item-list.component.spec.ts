import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppItemListComponent } from './app-item-list.component';
import { Store, StoreModule } from '@ngrx/store';

describe('AppItemListComponent', () => {
  let component: AppItemListComponent;
  let fixture: ComponentFixture<AppItemListComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ AppItemListComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppItemListComponent);
    component = fixture.componentInstance;
    store = TestBed.get<Store>(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
