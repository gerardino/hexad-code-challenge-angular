import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../model/Item.model';
import { Store } from '@ngrx/store';
import * as fromStore from '../store/reducers';
import * as itemActions from '../store/actions/items.actions';

@Component({
  selector: 'app-item-list',
  templateUrl: './app-item-list.component.html',
  styleUrls: ['./app-item-list.component.scss']
})
export class AppItemListComponent implements OnInit {

  @Input('item')
  item: Item;

  constructor(private store: Store<fromStore.State>){ }

  ngOnInit() { }

  rate(name, diff){
    this.store.dispatch(new itemActions.RateItem(name, diff));
  }

  rateUp(name) {
    this.rate(name, +1);
  }

  rateDown(name){
    this.rate(name, -1);
  }

}
