# HexadCodeChallengeAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.3.

## Running the project

### Prerrequisites

1. node.js
2. angular-cli (https://cli.angular.io/)

### Running

1. Run `ng serve` for the dev server. 
2. Navigate to `http://localhost:4200/`.

## Functionality

After running the project successfully, you should see the list of favourite items. To the right it has the rating number and buttons to up or down the rating.

Whenever the rating is changed, the items are ordered accordingly.

At the bottom there's the _Random Rating_ text with a button that toggles automatic addition in the rating values every 500ms. Clicking it once will start automatic rating and clicking it again will stop it.

## Design

The application consists of the `app-list-component` and `app-list-item-component` components. The list contains the items. The list is bound to the store which contains the items and a flag for the _random rating_.

The store contains reducers for setting up the items and updating the rating or the _random rating_ flag.

The actions match the reducers. For the _random rating_ three actions are used: `ToggleAutoRating` that indicates when the flag changes, `AddRandomRatings` that is triggered for the addition to happen and `RepeatRandomRatings` that contains the timer and checks the condition to repeat if the flag is true.

The `items-service` exists to load the items from a JSON file.